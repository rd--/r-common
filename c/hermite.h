#ifndef R_COMMON_C_HERMITE_H
#define R_COMMON_C_HERMITE_H

#define R float

R hermite(R x, R y0, R y1, R y2, R y3);

#endif

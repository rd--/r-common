#ifndef R_COMMON_C_FLOAT_H
#define R_COMMON_C_FLOAT_H

typedef float f32;
typedef double f64;

f64 f64_min(f64 p, f64 q);
f64 f64_max(f64 p, f64 q);

#endif

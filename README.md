r-common
--------

library of functions common to local projects

- [c](http://www.open-std.org/jtc1/sc22/wg14/)

tested-with:
[gcc](http://gcc.gnu.org/)-12.2.0
[clang](https://clang.llvm.org/)-14.0.6

debian:
libjack-jackd2-dev (libjack-dev);
freeglut3-dev libasound-dev libglu1-mesa-dev libpng-dev libsamplerate-dev libsndfile-dev

© [rohan drape](http://rohandrape.net/), 2003-2025, [gpl](http://gnu.org/copyleft/)
